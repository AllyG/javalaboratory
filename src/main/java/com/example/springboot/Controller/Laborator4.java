package com.example.springboot.Controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/abc")
public class Laborator4 {
    @GetMapping("/a")
    public ResponseEntity<String> a(Model model) {
        return new ResponseEntity<String>("root /abc/a", HttpStatus.OK);
    }

    @GetMapping("/b")
    public ResponseEntity<String> b(Model model) {
        return new ResponseEntity<String>("root /abc/b", HttpStatus.OK);
    }
}
