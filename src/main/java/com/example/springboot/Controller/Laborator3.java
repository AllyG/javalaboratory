package com.example.springboot.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Laborator3 {

    @GetMapping("/test/{a}")
    public ResponseEntity<String> home(@PathVariable(value = "a") Integer a ){

        if (a < 0) {
            return new ResponseEntity<String>("Eroare numerele trebuie sa fie nenegative!", HttpStatus.BAD_GATEWAY);
        }
        return new ResponseEntity<String>("Nu sunt rori", HttpStatus.OK);
    }


}
