package com.example.springboot.Controller;

import org.springframework.web.bind.annotation.*;

@RestController("/")
public class HelloController {

    @GetMapping("/")
    public String getMessage() {
        return "hello word";
    }

    @GetMapping("/n")
//    @ResponseStatus(HttpStatus.ACCEPTED)
    public String getMessageN(
            @RequestParam(value = "a", required=true) Integer a,
            @RequestParam(value = "b", required=false,  defaultValue = "default") String b,
            @RequestParam(value = "c", required=false) Double c
                              ) {

//        if (){}
///n?a=10&c=12.5
        return "works- a = " + a + " b = " + b + " c =  " +c ;
    }

    @GetMapping({"/m/{a}/{b}/{c}", "/m/{a}", "/m/{a}/{b}", "/m/{a}/{c}"})
    @ResponseBody
    public String getMessageM(
            @PathVariable (required=true) Integer a,
            @PathVariable(required=false) String b,
            @PathVariable (required=false) Double c
                              ) {

//        if (){}
///n?a=10&c=12.5
        return "works- a = " + a + " b = " + b + " c =  " +c ;
    }


}
